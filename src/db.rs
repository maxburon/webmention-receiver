use crate::AppState;
use crate::errors::WMError;
use crate::webmention::{APIWebmention, Webmention};

use serde::{Deserialize, Serialize};
use serde_json;
use microformats_parser::microformats::Root;
use mongodb::{
    Collection,
    // bson::{doc},
    bson::Document,
    bson::oid::ObjectId,
    error::Error
};
use webpage::{SchemaOrg, Webpage, HTML, HTTP};

fn create_storedhttp(http:HTTP) -> StoredHTTP {
    StoredHTTP {
        ip: http.ip,
        redirect_count: http.redirect_count as i32,
        content_type: http.content_type,
        response_code: http.response_code as i32,
        headers: http.headers,
        url: http.url,
        body: http.body
    }
}

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct StoredHTTP {
    pub ip: String,
    pub redirect_count: i32,
    pub content_type: String,
    pub response_code: i32,
    pub headers: Vec<String>,
    pub url: String,
    pub body: String,
}

fn remove_unsigned_type(json: &serde_json::Value)  -> serde_json::Value {
    match json {
        serde_json::Value::Number(num) =>  {
            if num.is_u64() {
                serde_json::Value::from(num.as_u64().unwrap() as f64)
            } else if num.is_i64() {
                serde_json::Value::from(num.as_i64().unwrap() as f64)
            } else {
                json.clone()
            }},
        serde_json::Value::Object(obj) => {
            let mut map = serde_json::Map::new();
            for (key,value) in obj.iter() {
                map.insert(key.to_string(), remove_unsigned_type(value));
            }
            serde_json::Value::from(map)
        },
        serde_json::Value::Array(arr) => {
            let mut vec = Vec::new();
            for value in arr {
                vec.push(remove_unsigned_type(&value));
            }
            serde_json::Value::from(vec)
        },
        _ => json.clone()
    }
}

fn create_storedwebpage(mut webpage : Webpage) -> StoredWebpage {
    let mut schemas = Vec::new();
    for s in webpage.html.schema_org {
        let value = remove_unsigned_type(&s.value);
        schemas.push(SchemaOrg {
            schema_type: s.schema_type,
            value: value
        });
    }
    
    webpage.html.schema_org = schemas;

    StoredWebpage {
        http: create_storedhttp(webpage.http),
        html: webpage.html
    }
}
#[derive(Debug)]
#[derive(Serialize, Deserialize)]
struct StoredWebpage {
    http: StoredHTTP,
    html: HTML
}

trait StorableWebmention {
    fn as_stored(self, webpage: Webpage, microformats: Root) -> StoredWebmention;
}

impl StorableWebmention for Webmention {
    fn as_stored(self, webpage: Webpage, microformats: Root) -> StoredWebmention {
        StoredWebmention {
            id: None,
            wm: self,
            webpage: create_storedwebpage(webpage),
            microformats: microformats
        }
    }
}

impl StoredWebmention {
    fn as_api(self) -> APIWebmention {
        APIWebmention {
            wm: self.wm,
            microformats: self.microformats,
            opengraph: self.webpage.html.opengraph,
            schema_org: self.webpage.html.schema_org,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct StoredWebmention {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    id: Option<bson::oid::ObjectId>,
    wm: Webmention,
    webpage: StoredWebpage,
    microformats: Root,
}

fn create_collection(state: &AppState) -> Collection<StoredWebmention> {
    state.client.database(&state.database)
        .collection_with_type::<StoredWebmention>(&state.collection)
}


async fn insert_one(state: &AppState, wm: StoredWebmention) -> Result<String, WMError> {

    let insert_res = create_collection(state)
        .insert_one(wm, None)
        .await;

    match insert_res {
        Ok(res) => {
            let id = res.inserted_id.as_object_id().expect("should be a object id").to_hex();
            Ok(id)
        },
        Err(e) => Err(WMError::InternalError{ mes: format!("{:?}",e) })
    }
}

async fn find_one(state: &AppState, doc:Document) -> Result<Option<StoredWebmention>, Error> {
    create_collection(state)
        .find_one(doc, None).await
}

pub async fn add(state: &AppState, wm: Webmention, webpage: Webpage, microformats: Root) -> Result<String, WMError> {
    insert_one(state, wm.as_stored(webpage, microformats)).await
}

pub async fn get_by_id(state: &AppState, id: String) -> Result<APIWebmention, WMError> {
    if let Ok(object_id) = ObjectId::with_string(&id) {
        let ans = find_one(state, doc! { "_id":  object_id }).await;
        match ans {
            Ok(Some(doc)) => Ok(doc.as_api()),
            Ok(None) => Err(WMError::NotFoundId{id : id}),
            _ => Err(WMError::InternalError{mes : format!("error during query of id {}", id)})
        }
    } else {
        Err(WMError::InternalError{mes : "error during id parsing".to_string()})
    }
}

pub async fn get_by_target(state: &AppState, target: String) -> Result<APIWebmention, WMError> {
    let ans = find_one(state, doc! { "wm.target": &target }).await;
    match ans {
        Ok(Some(doc)) => Ok(doc.as_api()),
        Ok(None) => Err(WMError::NotFound{url : target}),
        _ => Err(WMError::InternalError{mes : format!("error during query for target {}", target)})
    }
}
