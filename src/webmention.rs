use url::Url;
use microformats_parser::microformats::Root;
use webpage::opengraph::Opengraph;
use webpage::schema_org::SchemaOrg;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Webmention {
    pub source: Url,
    pub target: Url,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct APIWebmention {
    pub wm: Webmention,
    pub microformats: Root,
    pub opengraph: Opengraph,
    pub schema_org: Vec<SchemaOrg>,
        
}


