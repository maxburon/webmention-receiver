use crate::AppState;
use super::errors::WMError;
use super::webmention::Webmention;
use super::db;

use actix_web::{get, http::header, post, web, HttpResponse, ResponseError};
use microformats_parser::microformats;
use serde::{Deserialize, Serialize};
use url::Url;
use webpage::{Webpage, WebpageOptions};

#[derive(Deserialize, Serialize)]
pub struct FindParam {
    pub url: String
}

#[get("/webmention")]
async fn get_webmention(state: web::Data<AppState>, params: web::Query<FindParam>) -> HttpResponse {

    match db::get_by_target(&state, params.into_inner().url).await {
        Ok(wm) => HttpResponse::Ok().json(wm),
        Err(e) => e.error_response()
    }
}

#[derive(Deserialize, Serialize)]
pub struct IdParam {
    pub id: String
}

#[get("/webmention/{id}")]
async fn get_webmention_by_id(state: web::Data<AppState>, params: web::Path<IdParam>) -> HttpResponse {
    match db::get_by_id(&state, params.into_inner().id).await {
        Ok(wm) => HttpResponse::Ok().json(wm),
        Err(e) => e.error_response()
    }
}

#[derive(Deserialize, Serialize)]
struct WMForm {
    pub source: String,
    pub target: String
}

impl WMForm {
    fn parse(self) -> Result<Webmention, WMError> {

        if self.source.eq(&self.target) {
            return Err(WMError::SourceEqualsTarget{})
        }
        
        match Url::parse(&self.source) {
            Ok(source) => match Url::parse(&self.target) {
                Ok(target) => {
                    Ok(Webmention {
                        source: source,
                        target: target
                    })
                },
                _ => Err(WMError::UrlMisformed{ url: self.target })
            },
            _ => Err(WMError::UrlMisformed{ url: self.source })
        }
    }
}

#[post("/webmention")]
async fn post_webmention(state: web::Data<AppState>, params: web::Form<WMForm>) -> HttpResponse {

    match params.into_inner().parse() {
        Ok(wm) => {
            let options = WebpageOptions { allow_insecure: true, ..Default::default() };
            let info = Webpage::from_url(&wm.source.to_string(), options).expect("Halp, could not fetch");

            if !info.http.body.contains(&wm.target.to_string()) && !wm.source.domain().eq(&wm.target.domain()) {
                return WMError::MissingMention {target : wm.target.to_string()}.error_response()
            }
            let microformats = microformats::Root::from_string(String::from(&info.http.body)).unwrap();
            match db::add(&state, wm, info, microformats).await {
                Ok(id) => HttpResponse::Created()
                    .header(header::LOCATION, location_from_id(&state, id))
                    .finish(),
                Err(e) => e.error_response()
            }
        },
        Err(e) => e.error_response()
    }
    // let finder = getter::MediaGetter::new_find(params.into_inner());
    
    // match finder.find(&data).await {
    //     Ok(res) => HttpResponse::Ok().json(res),
    //     Err(e) => HttpResponse::Ok().json(format!("error !!! {:?}", e)),
    // }
}

fn location_from_id(state: &AppState, id: String) -> String {
    format!("{}/webmention/{}", state.host, id)
}


pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(get_webmention);
    cfg.service(get_webmention_by_id);
    cfg.service(post_webmention);
}
