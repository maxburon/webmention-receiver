use actix_web::{
    dev::HttpResponseBuilder, error, http::header, http::StatusCode, HttpResponse,
};

use derive_more::{Display, Error};

#[derive(Debug, Display, Error)]
pub enum WMError {
    #[display(fmt = "An internal error occurred. Failed with: \n {}", mes)]
    InternalError{ mes: String },
    #[display(fmt = "Webmention for {} is not found.", url)]
    NotFound { url: String },
    #[display(fmt = "Webmention of id {} does not exist.", id)]
    NotFoundId { id: String },
    #[display(fmt = "url {} is misformed.", url)]
    UrlMisformed { url: String },
    #[display(fmt = "source and target are equal.")]
    SourceEqualsTarget { },
    #[display(fmt = "missing mention of the source  in the target ({}).",  target)]
    MissingMention { target: String },

}

impl error::ResponseError for WMError {
    fn error_response(&self) -> HttpResponse {
        HttpResponseBuilder::new(self.status_code())
            .set_header(header::CONTENT_TYPE, "text/html; charset=utf-8")
            .body(self.to_string())
    }
    fn status_code(&self) -> StatusCode {
        match &*self {
            WMError::InternalError{ mes: _} => StatusCode::INTERNAL_SERVER_ERROR,
            WMError::NotFound { url: _} => StatusCode::NOT_FOUND,
            WMError::NotFoundId { id: _} => StatusCode::NOT_FOUND,
            WMError::UrlMisformed { url: _} => StatusCode::BAD_REQUEST,
            WMError::SourceEqualsTarget { } => StatusCode::BAD_REQUEST,
            WMError::MissingMention { .. } => StatusCode::BAD_REQUEST,
        }
    }
}

