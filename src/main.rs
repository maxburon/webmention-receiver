#[macro_use]
extern crate bson;
extern crate dotenv;

use actix_web::http::ContentEncoding;
use actix_web::{middleware, web, App, HttpServer};
use dotenv::dotenv;
use std::{env};
use mongodb::Client;
use mongodb::error::Error;
use mongodb::options::ClientOptions;



pub mod db;
pub mod errors;
pub mod webmention;

mod routes;

// This struct represents state
#[derive(Clone, Debug)]
pub struct AppState {
    pub client: Client,
    pub database: String,
    pub collection: String,
    pub host: String,
    pub port: String,
}

impl AppState {
    pub async fn new() -> Result<Self, Error> {
        let mut client_options = ClientOptions::parse("mongodb://127.0.0.1:27017").await?;
        client_options.app_name = Some("webmention".to_string());
        Ok(Self {
            client: Client::with_options(client_options)?,
            collection: env::var("COLLECTION").unwrap(),
            database: env::var("DATABASE").unwrap(),
            host: env::var("HOST").unwrap(),
            port: env::var("PORT").unwrap(),
        })
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    env::set_var("RUST_LOG", "actix_web=debug,actix_server=info");
    env_logger::init();

    // for (key, value) in env::vars() {
        // println!("{}: {}", key, value);
    // }


    // connection to db
    if let Ok(state) = AppState::new().await {
        HttpServer::new(move || {
            App::new()
                .data(state.clone())
                .wrap(middleware::Logger::default())
                .wrap(middleware::Compress::new(ContentEncoding::Br))
                .service(web::scope("").configure(routes::init_routes))
        })
            .bind(format!("0.0.0.0:{}", env::var("PORT").unwrap()))?
            .run()
            .await
    } else {
        Ok(())
    }
}
